「凱恩先生。昨天睡得好嗎？」
「是的，托您的福。」

凱恩真的睡得很香，好久沒睡得這麼好了，那是因為床比平時的床更軟，確實是這樣吧，雖然有點不好意思，但是我想我不能回想起昨晚的事情，但是我強烈地意識到艾琳娜是一位女性。

糟了！被像天使一樣溫柔的艾琳娜小姐擔心《雙頭毒蛇團》盯上的凱恩可能會在原來的旅館裏被埋伏。
雖然看到了被罪惡感所折磨的樣子還溫柔地安慰了我，但卻僅是如此而已⋯凱恩覺得很抱歉，讓他產生了不正經的妄想。

接下來想要洗個臉，走進盥洗室，發現女性內衣已經曬幹了，總有點在意的不得了。昨晚甚至有她對我說：「凱恩先生是個紳士，所以我很安心。」為了報答這種信任，我打算一定要注意不要出現那種氛圍⋯⋯⋯

「早餐可以和我一起吃嗎？」
「呃，艾琳娜小姐，也許你認為我有錢。」

在之前的惡龍中得到的六千金幣，坦率地說出全都用來償還教會的債務，凱恩向艾琳娜坦白了。

「啊、是這樣嗎？不管是做一人份的飯，還是做兩個人的飯都是一樣的。我還在這裏的時候你的生活方面上還請不要擔心。」
「即使你這麼說。」
「比起這些，請和我好好約定，在安全之前絶對不要外出賺錢，可不能想著去買東西啊！」

被那樣握住手認真地說的話，只能點頭「是」

「這是絶對的。現在開始稍微做飯了，但是在我不在的期間若凱恩不在的話我會哭的」
「我知道了⋯」

如果艾琳娜哭了，凱恩也會感到為難。之後，切成薄片後重新烤的麵包、湯、沙拉都附著的早餐之後，艾琳娜她準備匆匆忙忙地開始準備去公會工作。在這段期間，我該怎麼辦呢？
雖然會有一點空閑，但也會像個冒険家那樣整理工具，如果時間充裕的話也會考慮看一看植物圖鑒什麼的，這時來了客人。

───

「凱恩、凱恩你在哪裏？」
「希爾維亞小姐？」

高等精靈有的長耳朵，今天穿著裙子很短的可愛的修道服打扮的修女・希爾維亞來到了這裏。

「凱恩、謝謝你！」

凱恩突然被抱住。

「怎麼了？修女小姐？！」
「凱恩！你果然是我驕傲的兒子！」

希爾維亞緊緊地抱住凱恩不放，以前被這麼用力擁抱，是二十年以前還是孩子的事了。

「不、那個⋯⋯」
「希爾維亞小姐、凱恩先生很為難⋯」

哇的一聲，看著兩個人的艾琳娜咳嗽了。

「啊啦、對不起。這麼說來，凱恩已經是一個獨立的大人了。」
「說是大人，其實已經是個大叔⋯⋯」

凱恩不禁苦笑。
在生活在悠久歳月的希爾維亞看來，凱恩永遠都是一個孩子。

「呵呵，不過稍微好一點兒吧。小時候的凱恩，是個非常愛撒嬌的孩子。要是我每天晚上不抱你，就撒嬌說不睡⋯⋯」
「這是當孩子時三十年前的事情吧？」

凱恩至今還記得十五歳啟程時被依依不捨的希爾維亞緊緊抱住的情景，但已經幾乎不記得小時候曾經撒嬌的情景了。

「已經聽你說過幾萬次了，長大後要娶我做妻子？」
「不、我想我絶對沒有說過幾萬次，但那件事在艾琳娜面前⋯⋯」

在熟人面前，沒有比被父母提起小時候更丟臉的事情。然後艾琳娜嗤之以鼻的看著。

「這麼說來，從十三歳左右開始，凱恩就不怎麼黏我了。現在想來，那是⋯⋯」
「比起那個，妳那邊真的沒事吧？」

凱恩在孤兒院事故中遭受致命傷害之前，慌忙詢問情況。

「有人來冒険者公會問凱恩在哪裏時，我就告訴對方說就在公會這裏，然後對方就報說蛇頭被討伐了，也不知道凱恩竟然變得那麼強了。」
「討伐蛇頭！？」

凱恩轉向艾琳娜小姐問著。

「我去確認一下⋯」艾琳娜慌張地往公會的方向走去。

───

「凱恩先生！看來是真的⋯⋯昨晚淩晨似乎發生什麼。根據每人所目擊的不同，以蛇頭為首的《雙頭毒蛇團》所有人的惡行都被曝光了。」
「啊啊⋯⋯」

是誰幫我做了這樣的事情呢？凱恩覺得幫的某人就像是個英雄。

「然後，在那裏『以冒険者凱恩的之名討伐了這些惡棍！』據說有這樣一張大字的告示牌。是的、現在街道上都在議論著！」
「什麼！不、我才沒有做那種事！」

艾琳娜也進行了解釋。

「當然，我昨晚也一直都在一起，所以我知道。凱恩先生只有一次到廁所裏。但是，除了那個之外，我沒有離開房間。」
「這是⋯」

有不在場證明真是太好了。雖然《雙頭毒蛇團》毀滅是件好事，但就算將其歸咎於自己，凱恩也會感到困擾。

「啊啦、我還以為是凱恩已經打倒了？」
「不，希爾維亞小姐，妳也知道我並沒有那麼強大⋯」
「啊啦、但是很奇怪？」
「什麼？」
「就算艾琳娜在睡覺的時候會悄悄溜出來，凱恩也會這麼做也不奇怪吧？為什麼。不是以前一直和我在一起嗎？難道我會不知道嗎？」

不知道她在想些什麼，一邊自言自語著很奇怪，一邊笑瞇瞇地笑著的修女希爾維亞。凱恩連忙否認。

「不、不是的。因為沒有什麼奇怪的事情發生。」

因為睡在同一張床上，所以很難說什麼都沒發生，露出微妙表情的凱恩。希爾維亞目不轉睛地盯著凱恩的臉，嘆了一口氣。

「哈啊⋯⋯沒想到竟然和這麼漂亮的女性同床，真的什麼都沒做？」
「不是，艾琳娜只是擔心安全問題才幫我藏匿下的。」
「嗯、是啊，我知道了凱恩一直單身的原因。如果你真的有困難，可以找我商量，我會照顧好凱恩在孤兒院的妻子。」
「不、我是不會和教會的孤兒們結婚的。」
「既然如此，我可以還俗了。」

又是那個玩笑，凱恩苦笑著。

「結婚對象嘛，我自己也能找到。」

那個答案是完全沒有的，在養母希爾維亞面前，凱恩比想像中的還要有虛榮心。

「⋯⋯完全不是開玩笑的。」

希爾維亞的喃喃自語，太小了，根本聽不見。
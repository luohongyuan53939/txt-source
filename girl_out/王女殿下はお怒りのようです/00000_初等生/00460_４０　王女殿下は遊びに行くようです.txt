「完成了，大小姐！很漂亮啊。」

蕾蒂雪爾對滿臉歡喜的妮可兒露出了困惑的微笑。這一天，蕾蒂雪爾久違的在換衣間裡被妮可兒慢慢地精心打扮了。

「⋯總之，謝謝了。」
「好的，請慢慢享受。」

儘管暑假不用上學，卻還是如此盛裝打扮，這當然是有理由的。這一天，蕾蒂雪爾和朋友們約好要一起去街上逛。參加的成員有米蘭達蕾特、希爾梅斯和吉克四人。雖然也邀請了維蘿妮卡，不過，她好像必須出席茶會，時間衝突了。但相對的，她想要現在這個時代城裡流行的一種叫做什麼「坎托卡」的點心，打算買來給她當作禮物。
準備完成的蕾蒂雪爾和妮可兒一起來到大門口，路維克和克勞德已經在等待了。

「大小姐，請小心。」
「嗯，我知道。」

蕾蒂雪爾從口袋裡拿出兩枚胸針交給了打著招呼的路維克。

「上次整理換衣間的時候，又出現了不要的首飾，還請像往常一樣那麼辦吧。」
「我知道了，請交給我吧。」

蕾蒂希爾的零用錢是將多餘的行李賣給公爵家的商人埃里克得到的。果然不愧是公爵家，因為太礙事而處理掉的過於惡趣味的首飾，才賣了十個就得到了另人傻眼的金額。這些錢似乎完全足夠過平民生活了。

「那麼，我走了。」
「好的！慢走！」

傭人們將蕾蒂雪爾送出門外。因為是白天，天很遠，晴朗無雲，是個出門的好天氣。輕輕撩起隨風飄舞的頭髮，蕾蒂雪爾邁出了腳步。約好的地點在王都尼爾瓦恩的南門。雖然可以用轉移，但是不想引起路人的喧鬧，所以這次徒步前往。到了南門後，令人懷念的三人組已經等在那兒了。

「不好意思，久等了。」
「沒沒沒，是我們來早了。」

蕾蒂雪爾這麼一打招呼，米蘭達蕾特的瞳孔中就飛出了閃爍的光芒。她說她沒逛過街，大概是相當地期待吧。在旁邊，希爾梅斯則感動得顫抖著，匆忙地移開了視線。看來這邊比米蘭達蕾特更有幹勁啊。

「大家都到齊了，趕緊走吧。」
「嗯，拜託你帶路了，吉克。」
「好，交給我了。」

回答蕾蒂雪爾的吉克也開心地笑了起來。蕾蒂雪爾也為接下來的行程心跳不已。就這樣四個人開始了城內觀光。

「哇！」

希爾梅斯興奮的聲音在周圍迴響。

「吶，吉克，那邊的人在幹嘛啊？」
「啊，那個是連環畫劇。這個好像是兒童向的，但是也有成人向的。我也經常去看。」
「卡米西拜⋯⋯什麼意思？好吃嗎？」
「不是食物哦⋯」

對於看到什麼都覺得很新奇而頻頻發問的希爾梅斯，吉克只得苦笑著一一回答。

「鬧得好歡快哦。」
「沒辦法啊，利夫很期待今天啊。」

蕾蒂雪爾用難以形容的表情看著那樣的利夫。旁邊的米蘭達蕾特也苦笑著。但是蕾蒂雪爾並沒有漏掉，米蘭達蕾特從剛才開始一直在拚命觀察周圍的樣子。

「但是為什麼這麼開心呢？」
「當然了！啊，這個擺設很有趣⋯！這個叫什麼名字？」
「誰知道⋯這種時候就要叫專業人士了。吉克，稍微過來一會兒好嗎？」
「好的！現在就過去，請稍等！」

在吉克向米蘭達蕾特說明擺設的過程時，蕾蒂雪爾以自己的方式欣賞著街景，突然發現希爾梅斯在悠閑地看著大街。

「希爾梅斯，你在看什麼？」
「誒？沒啥，我就是在想，大街上的女人和貴族小姐們給人的印象完全不同。」

被這麼一說，蕾蒂雪爾看著路過的女性們，腦海中就浮現出學園裡女生們的身影。的確，市井婦女們的臉上的表情很明朗。雖然沒有貴族那樣華麗，但充滿了清爽活力。蕾蒂雪爾和希爾梅斯互相談論著感想，而米蘭達蕾特則目光炯炯地望向希爾梅斯。
之後米蘭達蕾特和希爾梅斯大聲喧鬧著，路過某劇場時，希爾梅斯再次發出了聲音。

「這個人真是個美人啊⋯！」
「她是在平民中是最受歡迎的女演員。」

希爾梅斯看著那個劇場入口旁邊貼著的海報。貴族和平民看的劇不一樣，認識的演員也不同。本來就對觀劇毫無興趣的希爾梅斯，只是純粹對初次見到的漂亮表演者們有興趣。

「⋯」

在看人氣女演員海報的希爾梅斯的背後，米蘭達蕾特的視線狠狠地盯著他。這是今天第二次的視線，蕾蒂雪爾交替地看了米蘭達蕾特和希爾梅斯。

「米拉，從剛才開始就用可怕的眼神看著希爾梅斯，怎麼了？」
「沒什麼⋯只是在想之後我有很多話不得不要問利夫。」

雖然覺得從米蘭達蕾特身上滲出了黑色的氣息，但是到達了下一個目的地後，這個話題就帶過去了。接下來到的是一個磚瓦結構的店。店前的花壇裡種植著草木，雖然古樸，但外觀上讓人感覺很心安。

「這裡是？」
「是我經常去的店。雖然是雜貨店，但是商品的質量很好，所以也經常看到貴族們來。」

吉克一邊說明著，一邊靜靜地推開了店門。嘎啦嘎啦的鈴聲響了起來。

「哇！」

緊接著鈴聲傳來的是年幼男孩的哭聲。不知發生了什麼就在店門口停了下來，蕾蒂雪爾悄悄地看了店內的情況。在裡面的櫃檯，年輕的男性拚命地安慰著６歳左右的男孩子。男孩蹲了下來，一邊道歉一邊抽抽搭搭地哭著，說著爸爸對不起了。

「店長，你好啊。」
「啊，吉克啊，歡迎觀臨。」
「怎麼了嗎？」

看著擔心地問著的吉克，店長的男子很不好地撓著頭。

「沒啥⋯⋯我家孩子把放在櫃檯上的鍾弄掉了⋯⋯」
「啊？確實店長說過是世世代代都非常重視的。」
「嗯，所以這孩子也感覺到了是他的責任⋯⋯」

追趕著吉克的蕾蒂雪爾，注視著櫃檯上放著的鍾。其大小是用雙手拿著正好合適的尺寸，從那塗料到處都剝落的樣子可以看出差不多進入了年限。與損壊了的主體一起的，大概是是零件的東西放在一起。像鋸齒狀的圓盤一樣的金屬有各種各樣大小的尺寸，還有其他完全不知道用在什麼地方的小部件。

「雖然很想拿去修理⋯⋯如果不再再多積累一點銷售額的話會很吃緊⋯⋯這樣亂七八糟也不知道能否修好⋯⋯」

吉克和店長在旁邊說著，蕾蒂雪爾則盯著這個壊掉的鍾。學校裡是用的鍾，蕾蒂雪爾在自己房間裡也有鍾，但是她完全沒見過這種鍾。聽說它完全不使用魔法就可以自己走。不使用魔法，是怎麼樣在這麼一個小盒子裡表示著早上和晚上的呢？

「姐姐？」

因為太認真思考了了，就連腳下傳來的小小的聲音都一瞬間聽漏了。視線向下看，剛才那個男孩用濕潤的眼睛抬頭看蕾蒂雪爾。

「怎麼了？」
「⋯⋯我，吧爸爸那重要的鍾摔壊了⋯⋯」

蕾蒂雪爾對著快要哭出來嘟噥著的男孩子睜大了眼睛。

「姐姐，怎樣才能修好呢？怎麼做才能把爸爸的鍾恢復原樣呢？」
「⋯」

看著那堅強的希望幫父親的男孩子的身姿，蕾蒂雪爾無法放置不理地脫口而出到。

「⋯⋯沒關係，我來修理。這樣一來，爸爸的鍾就會修好，也不用花錢，誰都不會為難。」
「真的？」
「嗯。」

男孩子用不可思議的表情歪著頭。蕾蒂雪爾彎下腰撫摸著男孩的頭，對著鍾使用魔術。淡淡的光包裹著鍾，就在光消失的時候，那裡有一塊修整得非常漂亮的鍾。但是，只是外表。

（⋯⋯針沒有動。果然不知道鍾的結構就修不好嗎？）

男孩從剛才就一直擔心地抬頭看著這邊。既想幫助他，同時對鍾也很感興趣。

（好，以此為契機來弄明白這個鍾的構造吧）

這樣決定後，蕾蒂雪爾對店長說道。

「那個，店長」
「嗯？怎麼了？」
「這個鍾可以寄存在我這兒嗎？」
「誒？不，但是⋯」
「不用擔心費用。我不打算收錢。我想修理它。」
「哈、哈⋯⋯雖然是令人感激的要求，但我不能給客人添麻煩⋯⋯」

店長好像在猶豫著。蕾蒂雪爾靜靜地等待著店長的回答，沒想到有人伸來援手。

「雖然也許情況不一樣，我也經常修理老家的紡織機，所以對機器很拿手。一直以來承蒙您的關照，我也會絞盡腦汁努力的。」

蕾蒂雪爾朝著吉克的方向望去，他好像注意到了視線似的朝這邊看，微微笑著點了點頭。

「⋯⋯我明白了。如果吉克都這麼說的話，請務必拜託了。」

對於意料之外的提案，店長雖然有些不安，但考慮到作為熟客的吉克和金錢問題，最終還是點了點頭。蕾蒂雪爾從店長那裡拿過了鍾，夾著它走向出口。離去的時候悄悄地回頭一看，剛才還在哭的男孩子一邊抽著鼻涕一邊向這邊揮手。看著他讓人不由得微笑，蕾蒂雪爾也笑著對他揮了揮手。

「走了很多路呢。」
「是啊。正好這附近有一家不錯的咖啡店。在那兒休息一下吧。」

街上人山人海，擁擠不堪。在人群中穿行著，蕾蒂雪爾和吉克好不容易到達了目的地的咖啡店前。但是在向四周東張西望的時候，卻看不見兩個朋友的身影。

「啊啦，那兩位呢？」
「一定是因為人多走散了吧。我們先進店吧。」

繼吉克之後，蕾蒂雪爾也進入了咖啡店。吉克和附近的店員說了些什麼，但不久就回到了這裡。

「已經和店員說了，請坐著稍等一會兒。我去找那兩個人。」

留下這句話後，吉克消失在店外。和吉克說話的店員把蕾蒂雪爾帶到座位上。坐在座位上再向店內環視一周。這是一家裝潢華麗、沉穩的咖啡店。入口正面有一個很大的櫥窗，看來是吧點心店和咖啡店融為一體了。

「再給我兩個這個。⋯⋯嗯？芽衣，怎麼了？」
「芽衣也要吃⋯」
「好吧。那麼再買一個吧。」

櫥櫃前的女性二人組在商談著些什麼。蜂蜜金色的短髮少女和栗色頭髮的馬尾少女。哎呀，好像在哪兒見過這兩人。

「⋯⋯啊！好久不見。」

在德雷希爾想起來之前，栗色頭髮的少女注意到了這邊。茶色的大大的瞳孔微笑著的她，符合開朗的美少女這個印象。

「以前，芽衣承蒙你關照了。謝謝你。」
「啊，希路美爾商會的⋯」
「是的。我叫阿夏，這邊的是芽衣。快點，打個招呼。」
「⋯」

在阿夏的催促下，芽衣默默地點了點頭。和初次見面時一樣，是一個完全不說話的孩子。

「好多點心啊。要開派對嗎？」
「啊，不是，是被人拜託了的。」
「是嗎？那個時候，你說在找人，找到了嗎？」
「啊，是！非常感謝。好好地找到了。」

就在蕾蒂雪爾和阿夏聊著輕鬆的閑話時，吉克帶著米蘭達蕾特和希爾梅斯進入了店內。

「是同行的人嗎？」
「嗯。」
「這樣啊，那我們就先走了。」

看到三個人來到這裡，阿夏只留下一句話就和芽衣走出了店。

「？」

希爾梅斯情不自禁地用眼睛追趕著經過自己身邊的二人組少女的身影。

「怎麼了？」
「不，那兩人──」
「笨蛋，利夫個笨蛋！」

米蘭達蕾特打了希爾梅斯一下。看著阿夏他們走出入口時希爾梅斯一瞬間驚呆了，察覺到米蘭達蕾特憤怒的表情。

「不對！不對，露露！你誤會了！等一下！」

雖然希爾梅斯慌忙地解釋，但是由於目前為止的過程，米蘭達蕾特完全的鬧彆扭了。

「哼，我再也不管利夫了。」
「兩位都請冷靜下來。好好地談一談吧！」

希爾梅斯追趕著鬧彆扭的米蘭達蕾特，吉克嘆息著跟在後面。在這熱鬧而又好笑的情景中，蕾蒂雪爾不由得露出了燦爛的笑容。

位於王都尼爾瓦恩的貴族街一角的菲利亞雷吉斯公爵邸裡。在那個房間裡，克莉絲塔一邊躺在沙發上一邊喝紅茶。從剛才開始就在專心喝，已經喝了第二杯了。

「啊，那個⋯⋯克莉絲塔大小姐。現在方便嗎？」

隨著敲門聲，克莉絲塔的侍女一邊小聲嘀咕著，一邊走進了房間。

「行了，怎麼？」
「薩利婭大小姐要見你。」
「誒？」

對於比自己大三歳的姐姐的來訪，克莉絲塔那圓圓的眼睛瞪得更加圓了。薩利婭去了未婚夫的領地，但是被父母叫回來了。已經回來了嗎？

「我馬上就過去。」
「我知道了。」

侍女點頭行禮後走了出去後，交錯而過的薩利婭走了進來。和母親長得很像的美麗大姐，一看到克莉絲塔，藤色的瞳孔就閃爍著。

「啊⋯⋯克莉斯！能打起精神來真是太好了！已經沒有什麼不舒服的地方了嗎？」
「薩利婭，歡迎回來。我已經沒事了」
「是嗎？那太好了⋯⋯」

克莉絲塔對緊緊抱住自己的薩利婭露出了困惑的微笑。一如既往的克莉絲塔的笑容讓薩利婭稍稍鬆了口氣，輕輕地撫摸著克莉絲塔的頭。